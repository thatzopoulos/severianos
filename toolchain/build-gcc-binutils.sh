#!/usr/bin/env bash
set -e
# This file will need to be run in bash, for now.


# === CONFIGURATION AND SETUP ===

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "DIR = $DIR"

ARCH=${ARCH:-"i686"}
TARGET="$ARCH-elf"
PREFIX="$DIR/cross/$ARCH" # In the future update this to $HOME/opt/cross possibly
export PATH="$PREFIX/bin:$PATH"

BUILD="$DIR/../build/$ARCH"
SYSROOT="$BUILD/root"

MAKE="make"
MD5SUM="md5sum"
NPROC="nproc"
REALPATH="realpath"

# Check if ginstall is installed, if not default to install
if command -v ginstall &>/dev/null; then
    INSTALL=ginstall
else
    INSTALL=install
fi

SYSTEM_NAME="$(uname -s)"

# Gross hack becuase the folder needs to exist before REALPATH will work, do i even need this?
mkdir -p ../build/i686
BUILD=$($REALPATH "$BUILD")

echo BUILD IS "$BUILD"
echo PREFIX is "$PREFIX"
echo SYSROOT is "$SYSROOT"


mkdir -p "$DIR/tarballs"
mkdir -p "$BUILD"


BINUTILS_VERSION="2.36.1"
BINUTILS_MD5SUM="3df9c3bbd944f9b57c1496f06741197b"
BINUTILS_NAME="binutils-$BINUTILS_VERSION"
BINUTILS_PKG="${BINUTILS_NAME}.tar.gz"
BINUTILS_BASE_URL="http://ftp.gnu.org/gnu/binutils"

# Note: If you bump the gcc version, you also have to update the matching
#       GCC_VERSION variable in the project's root CMakeLists.txt
GCC_VERSION="11.1.0"
GCC_MD5SUM="333068a65c119e74c9d7bfcc75a8eeba"
GCC_NAME="gcc-$GCC_VERSION"
GCC_PKG="${GCC_NAME}.tar.gz"
GCC_BASE_URL="http://ftp.gnu.org/gnu/gcc"


buildstep() {
    NAME=$1
    shift
    "$@" 2>&1 | sed $'s|^|\x1b[34m['"${NAME}"$']\x1b[39m |'
}


# === DOWNLOAD AND PATCH ===

pushd "$DIR/tarballs"
    md5="$($MD5SUM $BINUTILS_PKG | cut -f1 -d' ')"
    echo "bu md5='$md5'"
    if [ ! -e $BINUTILS_PKG ] || [ "$md5" != ${BINUTILS_MD5SUM} ] ; then
        rm -f $BINUTILS_PKG
        curl -LO "$BINUTILS_BASE_URL/$BINUTILS_PKG"
    else
        echo "Skipped downloading binutils"
    fi

    md5="$($MD5SUM ${GCC_PKG} | cut -f1 -d' ')"
    echo "gc md5='$md5'"
    if [ ! -e $GCC_PKG ] || [ "$md5" != ${GCC_MD5SUM} ] ; then
        rm -f $GCC_PKG
        curl -LO "$GCC_BASE_URL/$GCC_NAME/$GCC_PKG"
    else
        echo "Skipped downloading gcc"
    fi

    # if directoriy exists, then: 
    if [ -d ${BINUTILS_NAME} ]; then
        rm -rf "${BINUTILS_NAME}"
        rm -rf "$DIR/build/$ARCH/$BINUTILS_NAME"
    fi
    echo "Extracting binutils..."
    tar -xzf ${BINUTILS_PKG}


    # if directory exists, then: 
    if [ -d ${GCC_NAME} ]; then
        # Drop the previously patched extracted dir
        rm -rf "${GCC_NAME}"
        # Also drop the build dir
        rm -rf "$DIR/build/$ARCH/$GCC_NAME"
    fi
    echo "Extracting gcc..."
    tar -xzf $GCC_PKG
    
    if [ "$SYSTEM_NAME" = "Darwin" ]; then
        pushd "gcc-${GCC_VERSION}"
        ./contrib/download_prerequisites
        popd
    fi
popd


# === COMPILE AND INSTALL ===

rm -rf "$PREFIX"
mkdir -p "$PREFIX"

if [ -z "$MAKEJOBS" ]; then
    MAKEJOBS=$($NPROC)
fi

mkdir -p "$DIR/build/$ARCH"

pushd "$DIR/build/$ARCH"
    unset PKG_CONFIG_LIBDIR # Just in case

    rm -rf binutils
    mkdir -p binutils

    pushd binutils
        echo "XXX configure binutils"
        buildstep "binutils/configure" "$DIR"/tarballs/$BINUTILS_NAME/configure --prefix="$PREFIX" \
                                                 --target="$TARGET" \
                                                 --with-sysroot \
                                                 --disable-werror \
                                                 --disable-nls || exit 1
        if [ "$SYSTEM_NAME" = "Darwin" ]; then
            # under macOS generated makefiles are not resolving the "intl"
            # dependency properly to allow linking its own copy of
            # libintl when building with --enable-shared.
            buildstep "binutils/build" "$MAKE" -j "$MAKEJOBS" || true
            pushd intl
            buildstep "binutils/build" "$MAKE" all-yes
            popd
        fi
        echo "XXX build binutils"
        buildstep "binutils/build" "$MAKE" -j "$MAKEJOBS" || exit 1
        buildstep "binutils/install" "$MAKE" install || exit 1
    popd



    which -- $TARGET-as || echo $TARGET-as is not in the PATH

    rm -rf gcc
    mkdir -p gcc

    pushd gcc
      TEMPTARGET="$BUILD/temp"
      rm -rf "$TEMPTARGET"
      
      echo "XXX configure gcc and libgcc"
      REALTARGET="$PREFIX"


      buildstep "gcc/configure/" "$DIR"/tarballs/gcc-$GCC_VERSION/configure --prefix="$PREFIX" \
                                   --target="$TARGET" \
                                   --disable-nls \
                                   --enable-languages=c,c++ \
                                   --without-headers  || exit 1
        
      echo "XXX build gcc"
      buildstep "gcc/build" "$MAKE" -j "$MAKEJOBS" all-gcc || exit 1
      buildstep "gcc/build-target" "$MAKE" -j "$MAKEJOBS" all-target-libgcc || exit 1

      echo "XXX install gcc and libgcc"
      buildstep "gcc+libgcc/install" "$MAKE" DESTDIR="$TEMPTARGET" install-gcc install-target-libgcc || exit 1

     mkdir -p "$REALTARGET"
     cp -a "$TEMPTARGET"/"$PREFIX"/* "$REALTARGET/"
     rm -rf "$TEMPTARGET"
   popd

